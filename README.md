## Start the project

1. Double click the index.html file to start the project.

## Website introduction

The project is a cloth shop that let customer to buy clothes.

## Note about MIT license

The MIT license is one of the most permissive software licenses.It's a great choice because it allows you to share your code 
under a copyleft license without forcing others to expose their proprietary code, it’s business friendly and open source friendly 
while still allowing for monetization.